const Glue = require('glue');
const Hapi = require('hapi');
const Inert = require('inert');
const manifest = require('./config/manifest.js');
const Config = require('./config/enviroment.js')
const Db = require('./sqldb');


var start = () => {

    return Glue.compose(manifest, { relativeTo: __dirname })
      .then((server) => {
        server.log(['error', 'database', 'read']);

        // configure the views engine
        server.views({
          engines: {
            'html': {
              module: require('handlebars'),
              compileMode: 'sync' // engine specific
            }
          },
          layout: true,
          relativeTo: __dirname,
          path: 'templates',
          layoutPath: 'layouts',
          partialsPath:'partials',
          compileMode: 'async' // global setting
        });

        // Setup the session strategy
        server.auth.strategy('session', 'cookie', {
          password: Config.SESSION_SECRET, //Use something more secure in production
          redirectTo: false,
          isSecure: false // @TODO: set this field depending the enviroment setup (production / development...)
        });

        // Setup goole oauth
        server.auth.strategy('google', 'bell', {
          provider: 'google',
          password: Config.SESSION_SECRET, //Use something more secure in production
          clientId: Config.GOOGLE_ID,
          clientSecret: Config.GOOGLE_SECRET,
          location: Config.DOMAIN,
          isSecure: false
        });


        server.register(require('./app'))
        server.register({ register: require('./auth') }, {
          routes: {
              prefix: '/auth'
          }
        });

        // Serve assets static files
        server.route({
          method: 'GET',
          path: '/assets/{param*}',
          handler: {
            directory: {
              path: './assets',
              redirectToSlash: true,
              index: true
            }
          }
        });

        // Serve uploaded files
        server.route({
          method: 'GET',
          path: '/upload/{param*}',
          handler: {
            directory: {
              path: Config.UPLOAD_PATH,
              redirectToSlash: true,
              index: true
            }
          }
        });

        return server;

    }).then((server) => {
      return Db.sequelize.sync()
        .then(() => {
          // Let's go!
          server.start(); // can't return the server.start(), it will return a promise which doens'r resolve (the server is running though) 
          return server;
        });
    });
}

module.exports.start = start;