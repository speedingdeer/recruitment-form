// Load modules
const Code = require('code');
const Lab = require('lab');

const Db = require('../../sqldb');
const Utils = require('./utils')
// Test shortcuts

const lab = exports.lab = Lab.script();

lab.describe("App models", () => {

  var server = null;
  var user = null;


  // it doesn't need that long timeout but it't good to know there is such an option
  lab.before({ timeout: 2000 }, done =>  {
    Utils.create_user().then(u => {
      user = u;
      done();
    });
  });

  lab.it('Should allow to save a correct username', done => {

    user.update({name: "Lauren Mayberry"})
    .catch( () => {
      expect(false).to.be(true); // @TOdO: Should be just assert false or something. Find it.
    }).finally( () => {
      done();
    });

  });

  lab.it('Should NOT allow to save an empty name', done => {

    user.update({name: ""})
      .then( () => {
        expect(false).to.be(true);
      }).catch(() => { 
      }).finally( () => {
        done();
      });

  });

  lab.it('Should allow to save a correct user email', done => {

    user.update({email: "test@example.com"})
    .catch( () => {
      expect(false).to.be(true); // @TOdO: Should be just assert false or something. Find it.
    }).finally( () => {
      done();
    });

  });

  lab.it('Should NOT allow to save an empty user email', done => {

    user.update({email: ""})
      .then( () => {
        expect(false).to.be(true);
      }).catch(() => { 
      }).finally( () => {
        done();
      });

  });

  lab.it('Should NOT allow to save incorrect user email', done => {

    user.update({email: "text@example.com"})
      .then( () => {
        expect(false).to.be(true);
      }).catch(() => { 
      }).finally( () => {
        done();
      });

  });


  lab.it('Should allow to save a correct twitter username', done => {

    user.update({twitter_username: "@twitter"})
    .catch( () => {
      expect(false).to.be(true); // @TOdO: Should be just assert false or something. Find it.
    }).finally( () => {
      done();
    });

  });

  lab.it('Should NOT allow to save an empty twitter username', done => {

    user.update({twitter_username: ""})
      .then( () => {
        expect(false).to.be(true);
      }).catch(() => { 
      }).finally( () => {
        done();
      });

  });

  lab.it('Should NOT allow to save incorrect twitter username (no at)', done => {

    user.update({twitter_username: "twitter"})
      .then( () => {
        expect(false).to.be(true);
      }).catch(() => { 
      }).finally( () => {
        done();
      });

  });

    lab.it('Should NOT allow to save incorrect twitter username (at in the middle)', done => {

    user.update({twitter_username: "twit@ter"})
      .then( () => {
        expect(false).to.be(true);
      }).catch(() => { 
      }).finally( () => {
        done();
      });

  });

});