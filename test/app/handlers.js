// Load modules
const Code = require('code');
const Lab = require('lab');
const FormData = require('form-data');

const Server = require('../../server');
const Db = require('../../sqldb');
const Utils = require('./utils');
// Test shortcuts

const lab = exports.lab = Lab.script();

lab.describe("App handlers", () => {

  var server = null;
  var user = null;


  // it doesn't need that long timeout but it't good to know there is such an option
  lab.before({ timeout: 2000 }, done =>  {
    // start server first
    Server.start().then(s => {
      server = s;
      Utils.create_user().then(u => {
        user = u;
        done();
      });
    }); 
  });

  lab.it('Should always return 200 wheb hitting root page', done => {

    server.inject('/', response => {
      Code.expect(response.statusCode).to.equal(200);
      done();
    });

  });

  lab.it('Should return 401 if POST unauthenticated', done => {

    server.inject({ url: '/', method: 'POST' }, response => {
      Code.expect(response.statusCode).to.equal(401);
      done();
    });

  });

  lab.it('Should return 200 if POST authenticated', done => {

    server.inject({ url: '/', method: 'POST', credentials: { profile_id: user._id } }, response => {
      Code.expect(response.statusCode).to.equal(200);
      done();
    });

  });

  lab.it('Should return 200 when posting a valid form', done => {
 
    var form = new FormData();
    var form_string = "";
    form.append('name', 'filip');

    Utils.streamToPromise(form).then( payload => {
      server.inject({ url: '/', method: 'POST', payload: payload, headers: form.getHeaders(), credentials: { profile_id: user._id } }, response => {
        Code.expect(response.statusCode).to.equal(200);
        done();
      });
    });

  });

  lab.it('Should return 400 when posting an invalid form', done => {
 
    var form = new FormData();
    form.append('name', '');

    Utils.streamToPromise(form).then( payload => {
      server.inject({ url: '/', method: 'POST', payload: payload, headers: form.getHeaders(), credentials: { profile_id: user._id } }, response => {
        Code.expect(response.statusCode).to.equal(400);
        done();
      });
    });

  });

});