const Promise = require('bluebird');
const Db = require('../../sqldb');

const TEST_USER_GOOGLE_ID = 'test-goodle-id-1';
const TEST_USER_NAME = 'Alison Mosshart';

// helpers

module.exports.create_user = () => {
  return Db.User.destroy({ where: { google_id: TEST_USER_GOOGLE_ID } })
    .then( () => {
      return Db.User.create({
        google_id: TEST_USER_GOOGLE_ID,
        name: TEST_USER_NAME
      });
  });
}

// form data manipulation
// (inspired by npm stream-to-promise)

function streamToArray(stream, done) {
  if (!stream) {
    // no arguments, meaning stream = this
    stream = this
  } else if (typeof stream === 'function') {
    // stream = this, callback passed
    done = stream
    stream = this
  }

  var deferred
  if (!stream.readable) deferred = Promise.resolve([])
  else deferred = new Promise(function (resolve, reject) {
    // stream is already ended
    if (!stream.readable) return resolve([])

    var arr = []

    stream.on('data', onData)
    stream.on('end', onEnd)
    stream.on('error', onEnd)
    stream.on('close', onClose)

    function onData(doc) {
      arr.push(doc)
    }

    function onEnd(err) {
      if (err) reject(err)
      else resolve(arr)
      cleanup()
    }

    function onClose() {
      resolve(arr)
      cleanup()
    }

    function cleanup() {
      arr = null
      stream.removeListener('data', onData)
      stream.removeListener('end', onEnd)
      stream.removeListener('error', onEnd)
      stream.removeListener('close', onClose)
    }
  })

  if (typeof done === 'function') {
    deferred.then(function (arr) {
      process.nextTick(function() {
        done(null, arr)
      })
    }, done)
  }

  return deferred
}


function fromReadable (stream) {

  var promise = streamToArray(stream);
  // Ensure stream is in flowing mode
  if (stream.resume) { stream.resume(); }

  return promise.then(function concat (parts) {
    if (stream._readableState && stream._readableState.objectMode) {
      return parts
    }
    return Buffer.concat(parts.map(bufferize))
  });

}

function bufferize (chunk) {
  return Buffer.isBuffer(chunk) ? chunk : new Buffer(chunk)
}

module.exports.streamToPromise = (stream) => {
  if (stream.readable) { return fromReadable(stream); }
  return Promise.resolve()
}
