const Sequelize = require('sequelize');
const Config = require('./config/enviroment.js')

var db = {
  Sequelize: Sequelize,
  sequelize: new Sequelize(Config.DB_URI, Config.SEQUALIZE_OPTIONS)
};

db.User = db.sequelize.import('./app/models.js');

module.exports = db; 