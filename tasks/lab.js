const Gulp = require('gulp');
const Lab = require('gulp-lab');

Gulp.task('test', function () {
    return Gulp.src('test')
      .pipe(Lab('-v'));
});