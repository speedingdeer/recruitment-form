const Bower = require('gulp-bower');
const Gulp = require('gulp');

Gulp.task('bower', function() {
  return Bower();
});

Gulp.task('bower-files', function() {
    /** copy over semantic-ui build **/
    Gulp.src([
        './bower_components/recruitment-form-samantic-ui-build/semantic/dist/**/*',
    ])
    .pipe(Gulp.dest('./assets/lib/semantic-ui'));

    Gulp.src([
        './bower_components/fileapi/dist/*',
    ])
    .pipe(Gulp.dest('./assets/lib/FileAPI'));

    Gulp.src([
        './bower_components/jquery/dist/jquery.min.js',
    ])
    .pipe(Gulp.dest('./assets/lib/'));
    Gulp.src([
        './bower_components/animate.css/animate.min.css',
    ])
    .pipe(Gulp.dest('./assets/lib/'));
});