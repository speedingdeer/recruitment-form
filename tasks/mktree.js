'use strict';

const Gulp = require('gulp');
const Config = require('../config/enviroment');
const Fs = require('fs')
const Path = require('path')

var pictures_path = Path.join(Config.UPLOAD_PATH, Config.PICTURES_PATH);
var docs_path = Path.join(Config.UPLOAD_PATH, Config.DOCS_PATH);


Gulp.task('mktree', function() {
    if (!Fs.existsSync(Config.UPLOAD_PATH)) { Fs.mkdirSync(Config.UPLOAD_PATH); }
  if (!Fs.existsSync(pictures_path)) { Fs.mkdirSync(pictures_path); }
  if (!Fs.existsSync(docs_path)) { Fs.mkdirSync(docs_path); }
});
