module.exports = {

  SESSION_SECRET: '', // min 32 characters

  GOOGLE_ID: '',
  GOOGLE_SECRET: '',

  PORT: 8000,
  DOMAIN: '',

  DB_URI: 'sqlite://',
  SEQUALIZE_OPTIONS: {
    logging: false,
    storage: 'dev.sqlite',
    define: {
      timestamps: false
    }
  },

  UPLOAD_PATH: './upload',
  DOCS_PATH: 'docs',
  PICTURES_PATH: 'pictures'

};
