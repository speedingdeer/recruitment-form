const Config = require('./enviroment.js')

module.exports = {

  connections: [
    {
      host: "localhost",
      port: Config.PORT,
      routes: {
        cors: true
      },
      router: {
        stripTrailingSlash: true
      }
    }
  ],
  registrations: [
    { plugin: "vision" },
    { plugin: "inert" },
    { plugin: "hapi-auth-cookie" },
    { plugin: "bell" },
    {
      plugin: {
        register: "good",
        options: {
          ops: {
            interval: 60000
          },
          reporters: {
            console: [
              {
                module: "good-console",
                args: [
                  {
                    events: {
                      response: "*"
                    }
                  }
                ]
              },
              "stdout"
            ]
          }
        }
      }
    },
  ]
}