# Recruitment form demo app

## Running the app

### Required global dependencies

Install [Gulp] tasks manager and [Bower] package manager globally.

```sh
$ npm install -g gulp
$ npm install -g bower
```

### Optional but preferable

Install [Node Version Manager](nvm), install latest Node enviroment (v6.4), use .nvmrc file in the project root directory.

```sh
$ npm install nvm
$ nvm install 6.4
$ nvm use # (from the project root directory)
```

To run the app simply type in your terminal:

```sh
$ gulp serve
```

## Testing

To test the app simply type in your terminal:

```sh
$ gulp test
```

## Tasks / TODOs

### Types of tasks

There are two types of tasks.

  - **(F) feature** - The pieace of functionality which is going to be integrated into the app.
  - **(T) tech** - The tech setup.

### TODOs

- (T) Implement default 404 or redirect to root.
- ~~(T) Integrate Gulp or Grunt Task manager~~.
- ~~(T) Integrate LESS / SASS preprocessing task~~. 
- (T) Integrate JSLint task (not important now).
- ~~(T) Implement logout.~~
- (T) Trim form fields (epecially phone number, phone number is always a weird field).
- (T) Implement envoriments config (it's easy, they are just gulp tasks).
- ~~(T) Integrate Bower (within the task runner)~~(.
- (T) Integate server start / live reload task [low priority].
- ~~(T) Custom validation for LinkedIn / Github profiles (it's NOT just Url).~~
- ~~(T) Setup lab (functional) tests~~
- ~~(T) Setup tests (unit tests for models, access control).~~
- (T) Setup e2e tests.
- ~~(T) Inject user into template.~~
- ~~(T) Use bluebird promises instead of callbacks (promises are sexy).~~
- (T) Define build task (for production env - minify, combine, uglify, webpack maybe?).
- (T) Read list of countries from DB (not a file - it can easily lead to some data inconsistency).
- ~~(T) Integrate a data layer (Mongo/Redis, SQL? ...tbd)~~
- ~~(F) Implement the simplemest (v 0.0.1) form where user can put some details. (Will be easier to define other (F) tasks once we have something to talk about)~~
- (F) Don't expose submit untill there are any changes in form. (make it disabled untill then)
- ~~(F) Implement form's attachments (pdf? jpeg? any size contraints?)~~
- ~~(F) Implement changable profile images~~
- (F) Implement dome dynamic feature (list of projects mayb with some attachments?)
- ~~(F) implement log-in functionality (who is the form for? programmers? then github / twitter log-in maybe? (Twitter is risky, it's not on oAuth2.0 and doesn't expose users email. Must check on github))~~
- (F) Admin, reporting functionality (probably we won't get that far, but lets keep it in mind, might be easier to reproduce the normal SD cycle.)

[Node Version Manager]: https://github.com/creationix/nvm
[Gulp]: http://gulpjs.com/
[Bower]: https://bower.io/