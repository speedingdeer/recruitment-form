// The root / landing page plugin

var Google = require('./google');

exports.register = (plugin, options, next) => {

  plugin.route([
    { method: 'GET', path: '/google', config: Google.login },
    { method: 'GET', path: '/logout', handler: (request, reply) => {
        request.cookieAuth.clear();
        reply.redirect('/');
    }},
  ]);

  next();

};

exports.register.attributes = {
  name: 'auth'
};
