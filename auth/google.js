const db = require("../sqldb");

module.exports.login = {
  auth: 'google',
  handler: function (request, reply) {
    if (!request.auth.isAuthenticated) {
      return reply(Boom.unauthorized('Authentication failed: ' + request.auth.error.message));
    }

    var profile = request.auth.credentials.profile;
    db.User.find({ where: { 'google_id': profile.id } })
      .then(u => {
        if (u) {
          request.cookieAuth.set({
            profile_id: u._id
          });
          return reply.redirect('/');
        }

        user = db.User.build({
          name: profile.displayName,
          email: profile.email,
          picture_uri: profile.raw.picture,
          provider: 'google',
          google_id: profile.id
        });

        return user.save().then(u => {
          request.cookieAuth.set({
            profile_id: u._id
          });
          return reply.redirect('/');
        })
      });

  }
};