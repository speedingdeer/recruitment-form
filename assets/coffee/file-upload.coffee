$ ->

  MAX_IMAGE_SIZE = 512 # it's just profile image - doesnt't have to be big, actualyl 512 is still too big
  wrong_file_size_timout = null

  FileAPI.event.on $('#input-profile-image')[0], 'change', (evt) ->
    files = FileAPI.getFiles evt
    if !files.length then return; # if file not updated
    file = files[0]
    FileAPI.Image(file).resize(MAX_IMAGE_SIZE, MAX_IMAGE_SIZE, 'max').get (e, f) ->
      FileAPI.readAsDataURL f, (evt) ->
        if evt.type == 'load'
          $('#profile-image').css('background-image', 'url(' + evt.result + ')'); # show preview
          $('#recruitment-form input[name="picture_data"]').val(evt.result) # append to form
          $('#input-profile-image').val("")


  clean_local = () ->
    $("div#resume-link").removeClass "local" # can't be local if cancelled
    $("div#resume-link a.local span").html();
    $("div#resume-link").find("input[type=file]").val('')

  # disable local link
  $("div#resume-link a.local").click ->
    return false;

  FileAPI.event.on $('#input-profile-resume')[0], 'change', (evt) ->
    files = FileAPI.getFiles evt
    if !files.length
      clean_local();
      return

    file = files[0]
    if file.size > 3 * FileAPI.MB
      $('#recruitment-form').form('add errors', {
        'input-profile-resume': 'Your resume can\'t be bigger than 3MB',
      });
      clean_local();
      return;


    $("div#resume-link").addClass("local");
    $("div#resume-link a.local span").html(file.name);

