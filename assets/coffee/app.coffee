$ -> 

  $('#recruitment-form')
    .form({
      onSuccess: (evt) ->
          modal = $('.ui.basic.modal').modal({closable: false});
          modal.modal('show');
        ,
      onFailure: (evt) ->
          $('html, body').animate({
              scrollTop: $("body").offset().top
          }, 250)
          return false;
      on: 'change',
      fields: {
        name: {
          identifier  : 'name',
          rules: [
            {
              type   : 'empty',
              prompt : 'Please enter your name'
            }
          ]
        },
        email: {
          identifier  : 'email',
          rules: [
            {
              type   : 'email',
              prompt : 'Please enter a valid e-mail'
            }
          ]
        },
        phone_number: {
          identifier  : 'phone_number',
          optional: true,
          rules: [
            {
              type: 'regExp',
              value: /^[\+|00]\d{6,}$/,
              prompt : 'Please enter a valid phone number starting from \'+\' or \'00\', at least 6 digits'
            }
          ]
        },
        phone_number: {
          identifier  : 'phone_number',
          optional: true,
          rules: [
            {
              type: 'regExp',
              value: /^[\+|00]\d{6,}$/,
              prompt : 'Please enter a valid phone number starting from \'+\' or \'00\', at least 6 digits'
            }
          ]
        },
        twitter_username: {
          identifier  : 'twitter_username',
          optional: true,
          rules: [
            {
              type: 'regExp',
              value: /^@.+/,
              prompt : 'Please enter a valid Twiiter username starting from \'@\''
            }
          ]
        },
        country: { },
        city: { },
        postcode: { }
        linkedin_url: {
          optional: true,
          identifier  : 'linkedin_url',
          rules: [
            {
              type   : 'regExp',
              value  : /^(http(s)?:\/\/)?([\w]+\.)?linkedin\.com\/(pub|in|profile)\/.+/
              prompt : 'Please enter a valid LinkedIn profile url'
            }
          ]
        },
        github_url: {
          optional: true,
          identifier  : 'github_url',
          rules: [
            {
              type   : 'regExp',
              value  : /^(http(s)?:\/\/)?([\w]+\.)?github\.com\/.+/,
              prompt : 'Please enter a valid Github profile url'
            }
          ]
        },
        about: {
          optional: true,
          identifier  : 'about',
          rules: [
            {
              type   : 'maxLength[255]',
              prompt : 'Your text can\'t be longar than 255 characters'
            }
          ]
        },
        looking_for: {
          optional: true,
          identifier  : 'looking_for',
          rules: [
            {
              type   : 'maxLength[255]',
              prompt : 'Your text can\'t be longar than 255 characters'
            }
          ]
        }
      }
    })

  $('.ui.dropdown').dropdown();

