// The root / landing page plugin

var Handlers = require('./handlers');

exports.register = (plugin, options, next) => {

  plugin.route([
    { method: 'GET', path: '/', config: Handlers.show },
    { method: 'POST', path: '/', config: Handlers.update }
  ]);

  next();

};

exports.register.attributes = {
  name: 'root'
};