'use strict';

var authTypes = ['github', 'twitter', 'facebook', 'google'];

module.exports = function(sequelize, DataTypes) {
  var User = sequelize.define('User', {
    _id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
         notEmpty: true,
      }
    },
    phone_number: {
      type: DataTypes.STRING,
      allowNull: true,
      validate: {
        isValidPhoneNumber: function(value) {
          return /^[\+|00]\d{6,}$/.test(value)
        }
      }
    },
    email: {
      type: DataTypes.STRING,
      unique: {
        msg: 'The specified email address is already in use.'
      },
      validate: {
        isEmail: true
      }
    },
    role: {
      type: DataTypes.STRING,
      defaultValue: 'user'
    },

    picture_uri: DataTypes.STRING,
    resume_uri: DataTypes.STRING,
    provider: DataTypes.STRING,

    // @TODO make a big enum and load in on the client side to display countries
    country: DataTypes.STRING,
    city: DataTypes.STRING,
    postcode: DataTypes.STRING,

    about: {
      type: DataTypes.TEXT,
      allowNull: true,
      validate: {
        // it's a TEXT with validator not a String(255) to change it easier if needeed
        notTooLong: function(value) {
          return value.length < 255;
        }
      }
    },
    looking_for: {
      type: DataTypes.TEXT,
      allowNull: true,
      validate: { 
        // it's a TEXT with validator not a String(255) to change it easier if needeed
        notTooLong: function(value) {
          return value.length < 255;
        }
      }
    },
    /* social media profiles */
    linkedin_url: {
      type: DataTypes.STRING,
      allowNull: true,
      validate: {
        isUrl: true
      }
    },
    github_url: {
      type: DataTypes.STRING,
      allowNull: true,
      validate: {
        isUrl: true
      }
    },
    twitter_username: {
      type: DataTypes.STRING,
      allowNull: true,
      validate: {
         is: ["^@.+",'i'],
      }
    },
    /* provider id */
    facebook_id: { type: DataTypes.STRING, unique: true },
    twitter_id: { type: DataTypes.STRING, unique: true },
    google_id: { type: DataTypes.STRING, unique: true },
    github_id: { type: DataTypes.STRING, unique: true }

  });

  return User;
};
