const Db = require("../sqldb");
const Config = require('../config/enviroment.js')
const Promise = require("bluebird");
const Fs = require("fs");
const Path = require("path");
const Uuid = require("uuid");

function sign_handler(request) {

  return new Promise( (resolve) => {
    if(request.auth.credentials) {
      request.user = null;
        Db.User.findById(request.auth.credentials.profile_id)
          .then((u) => {
            request.user = u;
            resolve(request)
        });
    } else {
      resolve(request);
    }
  });

}

module.exports.show = {

  auth: {
    mode: 'try',
    strategy: 'session'
  },

  handler: (request, reply) => { 
    return sign_handler(request)
      .then((request) => {
        reply.view("home", { request: request });
      });

  }

}


module.exports.update = {

  auth: {
    mode: 'required',
    strategy: 'session'
  },

  payload: {
    maxBytes: 209715200, // @TODO sync this value with the server config (max upload size)
    output: 'stream',
    parse: true,
  },


  handler: (request, reply) => { 
    return sign_handler(request)
      .then((request) => {

        // @TODO: handle file save error
        if (request.payload.picture_data) {
          // if image updated save it
          var regex = /^data:.+\/(.+);base64,(.*)$/;
          var matches = request.payload.picture_data.match(regex);
          var ext = matches[1];
          var data = matches[2];
          var buffer = new Buffer(data, 'base64');
          var uuid = Uuid.v4()
          request.payload.picture_uri = Path.join(Config.UPLOAD_PATH, Config.PICTURES_PATH, uuid) + '.' + ext;
          Fs.writeFileSync(request.payload.picture_uri, buffer);
        }

        if(request.payload.resume_file && request.payload.resume_file.hapi.filename) {
          var ext = Path.extname(request.payload.resume_file.hapi.filename)
          request.payload.resume_uri = Path.join(Config.UPLOAD_PATH, Config.PICTURES_PATH, Uuid.v4()) + ext;
          request.payload.resume_file.pipe(Fs.createWriteStream(request.payload.resume_uri));
        }

        //@TODO: remove old files or implement separate garbage collector.

        var updates = {};
        for (var key in request.payload) {
          if (request.payload[key]) {
            updates[key] = request.payload[key];
          } else {
            updates[key] = null;
          }
        }

        request.user.update(updates)
          .then(() => {
            reply.view("home", { request: request })
          }).catch(() => {
            reply.view("home", { request: request }).code(400)
          });
      });
  }

}