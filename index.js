const Server = require("./server");
Server.start().then((server) => { console.log('✅  Server is listening on ' + server.info.uri.toLowerCase()); });
